package controllers

import (
	websitev1alpha1 "gitlab.com/tonytheleg/website-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// websiteLabels
func websiteLabels(w *websitev1alpha1.Website) map[string]string {
	label := map[string]string{"app": w.Name}
	return label
}

// websiteConfigMap
func (r *WebsiteReconciler) websiteConfigMap(w *websitev1alpha1.Website) *corev1.ConfigMap {
	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      w.Name + "-config",
			Namespace: w.Namespace,
			Labels:    websiteLabels(w),
		},
		Data: map[string]string{
			"MESSAGE": w.Spec.Content, // this gets set as the MESSAGE env var in the deployment
		},
	}
	controllerutil.SetControllerReference(w, cm, r.Scheme)
	return cm
}

// websiteService
func (r *WebsiteReconciler) websiteService(w *websitev1alpha1.Website) *corev1.Service {
	labels := websiteLabels(w)
	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      w.Name + "-service",
			Namespace: w.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{{
				Port:     w.Spec.ExposePort,
				Protocol: corev1.ProtocolTCP,
				TargetPort: intstr.IntOrString{
					IntVal: 8081,
				},
			}},
			Selector: labels,
		},
	}
	controllerutil.SetControllerReference(w, svc, r.Scheme)
	return svc
}

// websiteRoute

// websiteDeployent
func (r *WebsiteReconciler) websiteDeployment(w *websitev1alpha1.Website) *appsv1.Deployment {
	labels := websiteLabels(w)
	dep := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      w.Name,
			Namespace: w.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &w.Spec.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Image: w.Spec.ImageName,
							Name:  w.Name,
							EnvFrom: []corev1.EnvFromSource{{
								ConfigMapRef: &corev1.ConfigMapEnvSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: w.Name + "-config",
									},
								},
							},
							},
						},
					},
				},
			},
		},
	}
	controllerutil.SetControllerReference(w, dep, r.Scheme)
	return dep
}
