/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

/*
	cachev1alpha1 "github.com/swatishr/memcached-operator/pkg/apis/cache/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	appsv1 "k8s.io/api/apps/v1"
*/

import (
	"context"

	websitev1alpha1 "gitlab.com/tonytheleg/website-operator/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

var log = logf.Log.WithName("controller_website")

// WebsiteReconciler reconciles a Website object
type WebsiteReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=website.example.local,resources=websites,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=website.example.local,resources=websites/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=website.example.local,resources=websites/finalizers,verbs=update

/* These extra RBAC permissions below are needed for the controller to create the deployment, configmap, and service.
It also needed permissions to get pods to check status and replicas of stuff i found out.
*/
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Website object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *WebsiteReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", req.Namespace, "Request.Name", req.Name)
	reqLogger.Info("Reconciling Website")

	/* Flow of reconciliation:
	Typically, reconcile is triggered by a Controller in response to cluster Events
	(e.g. Creating, Updating, Deleting Kubernetes objects) or external Events
	(GitHub Webhooks, polling external sources, etc).

	Below, we get the website instance this was called against to compare its current state with desire state
	If its not found we need to create it by:
		Creating the configmap needed with the site context if it doesnt exist already
		Creating the deployment if it doesnt exist already
		Checking the number of replicas and making sure it matches
		Creating the service to expose it if it doesnt exist yet

	This probably needs some serious updating though because the point is to make sure
	the actual state matches desired state, this isnt totally accounting for it but it will
	*/

	// Get the Website instance
	instance := &websitev1alpha1.Website{}
	err := r.Client.Get(context.TODO(), req.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			reqLogger.Info("Website resource not found. Ignoring.")
			return ctrl.Result{}, nil
		}
		reqLogger.Info("Failed to get Website")
		return ctrl.Result{}, err
	}

	var result *ctrl.Result

	// Config Map
	result, err = r.reconcileConfigMap(ctrl.Request{}, instance, r.websiteConfigMap(instance))
	if result != nil {
		return *result, err
	}

	// Deployment
	result, err = r.reconcileDeployment(ctrl.Request{}, instance, r.websiteDeployment(instance))
	if result != nil {
		return *result, err
	}

	// Service
	result, err = r.reconcileService(ctrl.Request{}, instance, r.websiteService(instance))
	if result != nil {
		return *result, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *WebsiteReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&websitev1alpha1.Website{}).
		Complete(r)
}
