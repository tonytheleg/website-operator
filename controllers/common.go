package controllers

import (
	"context"

	websitev1alpha1 "gitlab.com/tonytheleg/website-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
)

// reconcileConfigMap creates a configMap storing the Content provided in the Website Spec and naming it after the object
func (r *WebsiteReconciler) reconcileConfigMap(req ctrl.Request, w *websitev1alpha1.Website, c *corev1.ConfigMap) (*ctrl.Result, error) {
	foundCm := &corev1.ConfigMap{}
	err := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      c.Name,
		Namespace: w.Namespace,
	}, foundCm)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Info("Creating a new ConfigMap", "ConfigMap.Namespace", c.Namespace, "ConfigMap.Name", c.Name)
			err = r.Client.Create(context.TODO(), c)
			if err != nil {
				log.Error(err, "Failed to create new ConfigMap", "ConfigMap.Namespace", c.Namespace, "ConfigMap.Name", c.Name)
				return &ctrl.Result{}, err
			} else {
				return nil, nil
			}
		} else if err != nil {
			log.Info("Failed to get ConfigMap")
			return &ctrl.Result{}, err
		}
	}
	return nil, nil
}

// reconcileService creates a ClusterIP service to load balance the pods and expose in pod network
func (r *WebsiteReconciler) reconcileService(req ctrl.Request, w *websitev1alpha1.Website, s *corev1.Service) (*ctrl.Result, error) {
	foundSvc := &corev1.Service{}
	err := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      s.Name,
		Namespace: w.Namespace,
	}, foundSvc)
	if err != nil {
		if errors.IsNotFound(err) {
			// See createService function below
			log.Info("Creating a new Service", "Service.Namespace", s.Namespace, "Service.Name", s.Name)
			err = r.Client.Create(context.TODO(), s)
			if err != nil {
				log.Error(err, "Failed to create new Service", "Service.Namespace", s.Namespace, "Service.Name", s.Name)
				return &ctrl.Result{}, err
			} else {
				return nil, nil
			}
		} else if err != nil {
			log.Info("Failed to get Service")
			return &ctrl.Result{}, err
		}
	}
	return nil, nil
}

// reconcileRoute

// reconcileDeployent
// createWebsiteDeployment creates a deployment, use the config map already created for the website content
//func (r *WebsiteReconciler) reconcileService(req ctrl.Request, w *websitev1alpha1.Website, s *corev1.Service) (*ctrl.Result, error) {
func (r *WebsiteReconciler) reconcileDeployment(req ctrl.Request, w *websitev1alpha1.Website, d *appsv1.Deployment) (*ctrl.Result, error) {
	foundDep := &appsv1.Deployment{}
	err := r.Client.Get(context.TODO(), types.NamespacedName{
		Name:      d.Name,
		Namespace: w.Namespace,
	}, foundDep)
	if err != nil {
		if errors.IsNotFound(err) {
			// See createWebsiteDeployment function below
			log.Info("Creating a new Deployment", "Deployment.Namespace", d.Namespace, "Deployment.Name", d.Name)
			err = r.Client.Create(context.TODO(), d)
			if err != nil {
				log.Error(err, "Failed to create new Deployment", "Deployment.Namespace", d.Namespace, "Deployment.Name", d.Name)
				return &ctrl.Result{}, err
			} else {
				return nil, nil
			}
		} else if err != nil {
			log.Info("Failed to get Deployment")
			return &ctrl.Result{}, err
		}
	}

	// Check deployment is size of spec
	size := w.Spec.Replicas
	if *foundDep.Spec.Replicas != size {
		foundDep.Spec.Replicas = &size
		err = r.Client.Update(context.TODO(), foundDep)
		if err != nil {
			log.Error(err, "Failed to update Deployment", "Deployment.Namespace", foundDep.Namespace, "Deployment.Name", foundDep.Name)
			return &ctrl.Result{}, err
		}
		return &ctrl.Result{Requeue: true}, nil
	}
	return nil, nil
}
