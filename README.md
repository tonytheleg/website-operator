# Website Operator

The Website Operator is just a sample test operator I made to better understand how to write operators. 

This operator adds a website kubernetes object that when created:
- Creates a Go HTTP server deployment with the number of replicas provided
- A config map to set what the website will print based on a message provided in Spec
- A service to load balance the pods (ClusterIP, no external access)

### GoWebsite Image
The GoWebsite image is a very basic http server that prints a default message OR the value of the MESSAGE environment variable, if set. The Dockerfile and code is in the hack directory of this repo.

# Useful info and learning
Check out the comments in the WebsiteSpec section of `website-operator/api/v1alpha1/website_types.go` and the Reconcile method of `website-operator/controllers/website_controller.go` to see more about how this is implemented.

### To build and use the image without Kubernetes
- Change to the hack directory: `cd website-operator/hack/gowebsite`
- Build the image with Docker or Podman: `podman build -t IMAGE_REGISTRY/NAME:VERSION .` (Chose a registry, name and version youd like)
- To run: `podman run -p 8081:8081 -e MESSAGE="This is what my site will print!" --name gowebsite IMAGE_REGISTRY/NAME:VERSION`

### To use the website-operator
_Note: if you are using podman, use the `podman.mak` file by specifying using the `-f` flag when running the below commands (`make -f podman.mak COMMAND`)_
Update the Makefile (or podman.mak if using podman) with:
- The VERSION (TAG_VERSION for podman) variable to the number of the image to use
- The IMAGE_TAG_BASE variable to your image registry where you want to push the operator controller (ie; docker.io/your-username/website-operator) 
- Make sure to generate needed files: `make generate; make manifests`
- Build the operator image and push: `make docker-build docker-push`
- Deploy the CRD to your cluster: `kubectl apply -f website-operator/config/crd/bases/website.example.local_websites.yaml`
- Deploy the controller: `make deploy`
- Check the operator is deployed: `kubectl get pods -n website-operator-system` -- All should be running
- Deploy a website, you can refer to the sample in `config/samples/website_v1alpha1_website.yaml` (Make sure to change imageName to the pull tag of the image you created and pushed earlier)

The website will be accessible from within a pod on port 8081. The pod and service are named after the Name you provide in the kubernetes manifest
```
apiVersion: website.example.local/v1alpha1
kind: Website
metadata:
  name: gowebsite  <----
```

To test: `kubectl exec -t POD-NAME -- curl http://NAME-service:EXPOSED-PORT` (All names/ports based on Spec provided)

### To Clean up
- Delete any websites deployed and the CRD 
- Delete controller: `make undeploy`


### Changes to come:
- Add OpenShift route name to Spec
- Check and make sure changes to website or its various needed objects work properly
- Deleting the config map doesnt recreate, find a way for all objects to be auto created if deleted outside of website object