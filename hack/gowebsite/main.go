package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("templates/tpl.gohtml"))
}

func main() {
	message := os.Getenv("MESSAGE")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if message == "" {
			tpl.Execute(w, "Congratulations, You've deployed a Go HTTP Server!")
		}
		tpl.Execute(w, message)
	})
	fmt.Println("starting server on port 8081")
	if err := http.ListenAndServe(":8081", nil); err != nil {
		log.Fatal("Failed to start webserver")
	}
}
